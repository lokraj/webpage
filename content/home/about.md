+++
# About/Biography widget.
widget = "about"
active = true
date = 2018-05-20T00:00:00

# Order that this section will appear in.
weight = 5

# List your academic interests.
[interests]
  interests = [
    "Vectored-vaccine development",
    "Viral pathogenesis",
    "Viral diagnostic assay development",
    "Reverse genetics",
    "Viral bioinformatics"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "PhD in Veterinary Microbiology"
  institution = "South Dakota State University"
  year = "--ongoing"

[[education.courses]]
  course = "MS Biological Science (Virology)"
  institution = "South Dakota State Univeristy"
  year = 2017

[[education.courses]]
  course = "Bachelor in Veterinary Science (equivalent to DVM)"
  institution = "Tribhuvan University, Nepal"
  year = 2013
 
+++

# Biography

I am Lok Raj Joshi, PhD student at [Dr. Diego Diel's Lab](http://www.diellab.net), Department of Veterinary and Biomedical Sciecne, SDSU. I am working on vectored-vaccine developemnt for various livestock diseases mainly focusing on swine diseases. I am also involved in the projects related to viral diagnostic assay development like indirect ELISA for picornavirus, production of polyclonal and monoclonal antibodies. Another important aspect of my research is viral pathogenesis. I have worked on Senecavirus A pathogenesis in swine.

I have deep  interest in next-generation sequencing and data analysis. I have been working on generating Miseq libraries and analyzing sequencing data with R and Linux.
